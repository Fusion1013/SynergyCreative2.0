package net.synergyserver.synergycreative.listeners;

import com.github.intellectualsites.plotsquared.bukkit.events.PlayerEnterPlotEvent;
import com.github.intellectualsites.plotsquared.bukkit.events.PlayerLeavePlotEvent;
import com.github.intellectualsites.plotsquared.bukkit.events.PlayerPlotDeniedEvent;
import com.github.intellectualsites.plotsquared.bukkit.util.BukkitUtil;
import com.github.intellectualsites.plotsquared.plot.commands.MainCommand;
import com.github.intellectualsites.plotsquared.plot.flag.Flags;
import com.github.intellectualsites.plotsquared.plot.object.Plot;
import com.github.intellectualsites.plotsquared.plot.util.PlotWeather;
import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.commands.CommandManager;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycreative.SynergyCreative;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Optional;
import java.util.UUID;

/**
 * Listens to all player-related events that involve PlotSquared.
 */
public class PlayerPlotListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMove(PlayerMoveEvent event) {
        // Ignore the event if it wasn't cancelled
        if (!event.isCancelled()) {
            return;
        }

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        // Ignore the event if they're not going to a plot
        Plot plot = Plot.getPlot(BukkitUtil.getLocation(event.getTo()));
        if (plot == null) {
            return;
        }

        // If the reason why the event was cancelled was that they're denied from the plot then send them a message
        Player player = event.getPlayer();
        if (PlotUtil.isDenied(player, plot)) {
            player.sendMessage(Message.format("events.plot_entry.error.denied"));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Plot plot = Plot.getPlot(BukkitUtil.getLocation(event.getTo()));

        // Ignore the event if the player isn't teleporting to a plot
        if (plot == null) {
            return;
        }

        // If the player is denied from the plot then change the teleport destination to the plot home
        Player player = event.getPlayer();
        if (PlotUtil.isDenied(player, plot)) {
            event.setTo(BukkitUtil.getLocation(plot.getHome()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerEnterPlot(PlayerEnterPlotEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        Plot plot = event.getPlot();

        // Loop through all players online and check if they're riding the given player
        for (Player p : Bukkit.getOnlinePlayers()) {
            // Ignore them if they're not riding the player of the event
            if (p.getSpectatorTarget() == null || !p.getSpectatorTarget().equals(player)) {
                continue;
            }

            // If the spectator is denied from the plot that is being entered, kick them out
            if (plot.isDenied(p.getUniqueId()) & !p.hasPermission("plots.admin.entry.denied")) {
                p.setSpectatorTarget(null);
                p.sendMessage(Message.format("events.plot_entry.error.denied"));
            }
        }

        // If the plot has a specified time, set the player's time to that because SynergyCore's personaltime listener
        // overrides PlotSquared's initial listener
        Optional<Long> timeFlag = plot.getFlag(Flags.TIME);
        if (timeFlag.isPresent()) {
            long personalTime = timeFlag.get();
            // Schedule it for the next tick because SynergyCore's listener is already on the highest priority
            Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), () -> {
                player.setPlayerTime(personalTime, false);
            }, 0L);
        }

        // If the plot has a specified weather, set the player's weather to that because SynergyCore's personalweather listener
        // overrides PlotSquared's initial listener
        Optional<PlotWeather> weatherFlag = plot.getFlag(Flags.WEATHER);
        if (weatherFlag.isPresent()) {
            WeatherType personalWeather = PlotUtil.getWeatherType(weatherFlag.get());
            // Schedule it for the next tick because SynergyCore's listener is already on the highest priority
            Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), () -> {
                // If it's null then reset the player's weather on the plot
                if (personalWeather == null) {
                    player.resetPlayerWeather();
                } else {
                    // Otherwise set it to the plot's weather setting
                    player.setPlayerWeather(personalWeather.toBukkitWeatherType());
                }
            }, 0L);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLeavePlot(PlayerLeavePlotEvent event) {
        Player player = event.getPlayer();

        // If the plot has a specified time, reset the player's time when they leave
        if (event.getPlot().getFlag(Flags.TIME).isPresent()) {
            // Schedule it for the next tick because SynergyCore's listener is already on the highest priority
            Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), () -> {
                // If the player left then ignore
                if (!player.isOnline()) {
                    return;
                }

                Long playerTime = PlayerUtil.getProfile(player).getCurrentWorldGroupProfile().getPersonalTime();
                if (playerTime == null) {
                    player.resetPlayerTime();
                } else {
                    player.setPlayerTime(playerTime, false);
                }
            }, 0L);
        }

        // If the plot has a specified weather, reset the player's time when they leave
        if (event.getPlot().getFlag(Flags.WEATHER).isPresent()) {
            // Schedule it for the next tick because SynergyCore's listener is already on the highest priority
            Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), () -> {
                // If the player left then ignore
                if (!player.isOnline()) {
                    return;
                }

                WeatherType personalWeather = PlayerUtil.getProfile(player).getCurrentWorldGroupProfile().getPersonalWeather();
                if (personalWeather == null) {
                    player.resetPlayerWeather();
                } else {
                    player.setPlayerWeather(personalWeather.toBukkitWeatherType());
                }
            }, 0L);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDenied(PlayerPlotDeniedEvent event) {
        UUID pID = event.getPlayer();

        // Ignore the event if the player is offline
        if (!PlayerUtil.isOnline(pID)) {
            return;
        }

        Player player = Bukkit.getPlayer(pID);
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // If they were undenied instead of denied then ignore the event
        if (!event.wasAdded()) {
            return;
        }
        // If the player isn't spectating someone then ignore the event
        if (player.getSpectatorTarget() == null) {
            return;
        }
        // If the player is not in the plot that they were denied from then ignore the event
        Plot plot = event.getPlot();
        if (!BukkitUtil.getPlayer(player).getCurrentPlot().equals(plot)) {
            return;
        }
        // If the player has bypass perms then ignore the event
        if (player.hasPermission("plots.admin.entry.denied")) {
            return;
        }

        // Otherwise, kick them out
        player.setSpectatorTarget(null);
        player.teleport(BukkitUtil.getLocation(plot.getDefaultHome()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String[] parsedCommand = event.getMessage().split(" ");
        String commandLabel = parsedCommand[0].replaceFirst("/", "");

        // If there isn't at least 2 arguments + the command label, then ignore the event
        if (parsedCommand.length < 3) {
            return;
        }

        CommandManager cm = CommandManager.getInstance();
        Command command = cm.getCommand(commandLabel);

        // Ignore the command if it's not valid
        if (command == null) {
            return;
        }

        // If it isn't a command from PlotSquared then ignore the event
        if (!cm.areCommandsEqual(commandLabel, "plot")) {
            return;
        }

        // If the subcommand isn't some form of home (visit) then ignore the event
        String[] parsedSubCommand = parsedCommand[1].split(":");
        if (parsedSubCommand.length == 0) {
            return;
        }
        MainCommand plotCM = MainCommand.getInstance();
        com.github.intellectualsites.plotsquared.commands.Command subCommand = plotCM.getCommand(parsedSubCommand[0]);

        // Ignore the subcommand if it's not valid
        if (subCommand == null) {
            return;
        }

        if (!subCommand.equals(plotCM.getCommand("visit"))) {
            return;
        }

        // If the home number entered is a valid number then save the home number, otherwise ignore the event
        int plotNumber = 1;
        if (parsedSubCommand.length == 2 && MathUtil.isInteger(parsedSubCommand[1])) {
            plotNumber = Integer.parseInt(parsedSubCommand[1]);
        } else if (parsedCommand.length == 4 && MathUtil.isInteger(parsedCommand[3])) {
            plotNumber = Integer.parseInt(parsedCommand[3]);
        }

        // Figure out the target player
        UUID targetPlayer = PlayerUtil.getUUID(parsedCommand[2], true, player.hasPermission("vanish.see"), false);
        // If there is no matching player then ignore the event
        if (targetPlayer == null) {
            return;
        }

        // Change the command executed to be with the matched player
        event.setMessage("/p h:" + plotNumber + " " + PlayerUtil.getName(targetPlayer));
    }

}
