package net.synergyserver.synergycreative.listeners;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TextBindToSignEvent;
import net.synergyserver.synergycore.events.WorldGroupProfileCreateEvent;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycreative.CreativeWorldGroupProfile;
import net.synergyserver.synergycreative.SynergyCreative;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 * Listens to various events emitted by <code>SynergyPlugin</code>s.
 */
public class SynergyListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onWorldGroupProfileCreate(WorldGroupProfileCreateEvent event) {
        WorldGroupProfile wgp = event.getNewWGP();

        // Ignore the event if the WGP isn't for this plugin
        if (!wgp.getWorldGroupName().equals(SynergyCreative.getWorldGroupName())) {
            return;
        }

        // Inject a LobbyworldGroupProfile into the event
        event.setNewWGP(new CreativeWorldGroupProfile(wgp));
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onTextBindToSign(TextBindToSignEvent event) {
        Sign sign = event.getSign();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(sign.getWorld().getName())) {
            return;
        }

        Player player = event.getPlayer();

        // Cancel the binding if the player is attempting it on a plot they're not added to
        if (!PlotUtil.canBuild(player, sign.getLocation())) {
            player.sendMessage(Message.get("events.text_binding_to_sign.error.not_added_to_plot"));
            event.setCancelled(true);
        }
    }

}
