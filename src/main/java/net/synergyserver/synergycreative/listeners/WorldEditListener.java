package net.synergyserver.synergycreative.listeners;

import com.sk89q.worldedit.event.extent.EditSessionEvent;
import com.sk89q.worldedit.util.eventbus.EventHandler;
import com.sk89q.worldedit.util.eventbus.Subscribe;
import net.synergyserver.synergycreative.SynergyCreative;
import net.synergyserver.synergycreative.inputs.InputMaskingExtent;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * Listens to WorldEdit-related events.
 */
public class WorldEditListener {

    @Subscribe(priority = EventHandler.Priority.NORMAL)
    public void onEditSession(EditSessionEvent event) {
        // Ignore the event if for some reason the world is null
        if (event.getWorld() == null) {
            return;
        }

        // Get the Bukkit world object of this event
        World world = Bukkit.getWorld(event.getWorld().getName());

        // Ignore the event if for some reason the world is null
        if (world == null) {
            return;
        }

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(world.getName())) {
            return;
        }

        // Attempt to get the player that is performing the operation
        Player player = null;
        if (event.getActor() != null && event.getActor().isPlayer()) {
            player = Bukkit.getPlayer(event.getActor().getUniqueId());
        }

        // Take over the "extent" to mask Inputs from being affected
        event.setExtent(new InputMaskingExtent(event.getExtent(), world, player));
    }

}
