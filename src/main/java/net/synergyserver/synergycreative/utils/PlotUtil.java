package net.synergyserver.synergycreative.utils;

import com.github.intellectualsites.plotsquared.bukkit.util.BukkitUtil;
import com.github.intellectualsites.plotsquared.plot.object.Plot;
import com.github.intellectualsites.plotsquared.plot.util.PlotWeather;
import net.synergyserver.synergycore.WeatherType;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Utility that handles plots.
 */
public class PlotUtil {

    /**
     * Checks whether a player can build at the specified location according to the plot protection.
     *
     * @param player The player to check the permissions of.
     * @param location The location that is attempted to be modified by the player.
     * @return True if the player has permission to build at the location.
     */
    public static boolean canBuild(Player player, Location location) {
        UUID pid = player.getUniqueId();
        com.github.intellectualsites.plotsquared.plot.object.Location psLocation = BukkitUtil.getLocation(location);
        Plot plot = Plot.getPlot(psLocation);

        if (plot == null || plot.isAdded(pid) || player.hasPermission("plots.admin.build")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if a player is denied from the given plot.
     *
     * @param player The player to check the denied status of.
     * @param plot The plot that the player is attemping to enter.
     * @return True if the player is denied from the plot.
     */
    public static boolean isDenied(Player player, Plot plot) {
        return plot.isDenied(player.getUniqueId()) && !player.hasPermission("plots.admin.entry.denied");
    }

    /**
     * Gets the <code>WeatherType</code> of a PlotSquared <code>PlotWeather</code> object.
     *
     * @param plotWeather The PlotSquared representation of weather.
     * @return The <code>WeatherType</code> of the weather input, or null if no match could be made.
     */
    public static WeatherType getWeatherType(PlotWeather plotWeather) {
        switch (plotWeather) {
            case RAIN:
                return WeatherType.RAIN;
            case CLEAR:
                return WeatherType.CLEAR;
            case RESET:
                return null;
            default: return WeatherType.CLEAR;
        }
    }
}
