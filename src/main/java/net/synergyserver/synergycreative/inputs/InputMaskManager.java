package net.synergyserver.synergycreative.inputs;

import com.boydti.fawe.object.FawePlayer;
import com.boydti.fawe.object.collection.BlockVectorSet;
import com.boydti.fawe.regions.FaweMask;
import com.boydti.fawe.regions.FaweMaskManager;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.AbstractRegion;
import com.sk89q.worldedit.regions.RegionOperationException;
import com.sk89q.worldedit.world.World;
import net.synergyserver.synergycreative.SynergyCreative;
import org.bukkit.Location;

import java.util.HashMap;

/**
 * Represents the mask manager used to mask <code>Inputs</code> from WorldEdit operations.
 */
public class InputMaskManager extends FaweMaskManager {

    private static InputMaskManager instance;
    private HashMap<World, InputRegion> inputRegions = new HashMap<>();

    /**
     * Creates a new <code>InputMaskManager</code> object.
     */
    private InputMaskManager() {
        super(SynergyCreative.getPlugin().getName());
    }

    /**
     * Returns the object representing this <code>InputMaskManager</code>.
     *
     * @return The object of this class.
     */
    public static InputMaskManager getInstance() {
        if (instance == null) {
            instance = new InputMaskManager();
        }
        return instance;
    }

    /**
     * Adds an <code>Input</code> to the regions used for WorldEdit masking.
     *
     * @param input The <code>Input</code> to add.
     */
    public void addInput(Input input) {
        // Try get the InputRegion for the world of the input
        World world = new BukkitWorld(input.getLocation().getWorld());
        InputRegion inputRegion = inputRegions.get(world);
        // If a region doesn't already exist, then create a new one
        if (inputRegion == null) {
            inputRegion = new InputRegion(world);
            inputRegions.put(world, inputRegion);
        }

        // Add the input to the region
        inputRegion.addInput(input);
    }
    @Override
    public FaweMask getMask(FawePlayer player, MaskType type) {
        InputRegion region = inputRegions.get(player.getWorld());
        if (region == null) {
            return null;
        } else {
            return new FaweMask(region, getKey());
        }
    }

    /**
     * Simplified copy of FAWE's FuzzyRegion
     */
    public class InputRegion extends AbstractRegion {
        private BlockVectorSet maskedLocations = new BlockVectorSet();
        private int minX;
        private int minY;
        private int minZ;
        private int maxX;
        private int maxY;
        private int maxZ;

        private InputRegion(World world) {
            super(world);
        }

        /**
         * Adds an <code>Input</code> to this <code>InputRegion</code>.
         *
         * @param input The <code>Input</code> to add.
         */
        private void addInput(Input input) {
            Location loc = input.getLocation();
            maskedLocations.add(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
            updateMinMax(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        }

        /**
         * Updates the minimum and maximum positions with the given new coordinates.
         *
         * @param x The x value of the new location.
         * @param y The y value of the new location.
         * @param z The z value of the new location.
         */
        private void updateMinMax(int x, int y, int z) {
            if (x > this.maxX) {
                this.maxX = x;
            }

            if (x < this.minX) {
                this.minX = x;
            }

            if (y > this.maxY) {
                this.maxY = y;
            }

            if (y < this.minY) {
                this.minY = y;
            }

            if (z > this.maxZ) {
                this.maxZ = z;
            }

            if (z < this.minZ) {
                this.minZ = z;
            }

        }

        @Override
        public BlockVector3 getMinimumPoint() {
            return BlockVector3.at(this.minX, this.minY, this.minZ);
        }

        @Override
        public BlockVector3 getMaximumPoint() {
            return BlockVector3.at(this.maxX, this.maxY, this.maxZ);
        }

        @Override
        public void expand(BlockVector3... changes) throws RegionOperationException {
            throw new RegionOperationException("Selection cannot expand");
        }

        @Override
        public void contract(BlockVector3... changes) throws RegionOperationException {
            throw new RegionOperationException("Selection cannot contract");
        }

        @Override
        public void shift(BlockVector3 change) throws RegionOperationException {
            throw new RegionOperationException("Selection cannot be shifted");
        }

        /**
         * Due to the nature of this mask and the purpose being to prevent Inputs from being changed,
         * the result of method is inverted from what the region actually contains.
         *
         * @param position The position to check.
         * @return True if there is *not* a location at the given position.
         */
        @Override
        public boolean contains(BlockVector3 position) {
            return maskedLocations.contains(position.getBlockX(), position.getBlockY(), position.getBlockZ());
        }
    }


}
