package net.synergyserver.synergycreative.inputs;

import com.github.intellectualsites.plotsquared.bukkit.util.BukkitUtil;
import com.github.intellectualsites.plotsquared.plot.object.Plot;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

@Embedded
public class CooldownInputFunction extends InputFunction {

    @Property("t")
    private int time;
    @Property("b")
    private UserGroup bypassGroup;
    @Property("tr")
    private long timeRemainder;

    @Transient
    private boolean bypassedCooldown = false;
    @Transient
    private long timeLoaded;

    /**
     * Required constructor for Morphia to work.
     */
    public CooldownInputFunction() {}

    /**
     * Creates a new <code>CooldownInputFunction</code> with the given parameters.
     *
     * @param input The <code>Input</code> that this <code>CooldownInputFunction</code> is registered to.
     * @param time The time in milliseconds that this <code>CooldownInputFunction</code> waits before being able to be activated again.
     * @param bypassGroup The <code>UserGroup</code> that is allowed to bypass the cooldown.
     */
    public CooldownInputFunction(Input input, int time, UserGroup bypassGroup) {
        super(input);
        this.time = time;
        this.bypassGroup = bypassGroup;
        this.timeRemainder = 0;
    }

    /**
     * Gets the time remaining in milliseconds until this <code>CooldownInputFunction</code>'s cooldown ends.
     *
     * @return The time until the cooldown ends.
     */
    public long getRemainingTime() {
        long currentTime = System.currentTimeMillis();
        long timeSinceActivation = currentTime - getInput().getLastUsed();
        long timeSinceLoad = currentTime - timeLoaded;

        // Return whichever has more delay: The time remainder from being unloaded or the time remaining from normal activation
        long maxRemainingTime = Math.max(timeRemainder - timeSinceLoad, time - timeSinceActivation);
        return Math.max(maxRemainingTime, 0);
    }

    @Override
    public String getType() {
        return "cooldown";
    }

    @Override
    public String getDescription() {
        long timeInTicks = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, time);
        long remainingTimeInTicks = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, getRemainingTime());
        return Message.format("inputs.cooldown.description", bypassGroup.toString(), timeInTicks, remainingTimeInTicks);
    }

    @Override
    public InputPriorityLevel getPriority() {
        return InputPriorityLevel.MEDIUM;
    }

    @Override
    public boolean shouldActivate(Entity entity, Event event) {
        // Reset the bypassCooldown value
        bypassedCooldown = false;

        long remainingTime = getRemainingTime();

        // If the cooldown period already ended then do nothing
        if (remainingTime == 0) {
            return true;
        }

        // If the activating entity was a player and they have bypass perms then bypass the cooldown
        if (entity != null && entity instanceof Player) {
            Player player = (Player) entity;
            Plot plot = Plot.getPlot(BukkitUtil.getLocation(getInput().getLocation()));

            switch (bypassGroup) {
                case PLOT_MEMBERS:
                    // If they are a plot member then bypass the cooldown
                    if (plot != null && plot.getMembers().contains(player.getUniqueId())) {
                        bypassedCooldown = true;
                        return true;
                    }
                case PLOT_TRUSTED:
                    // If they are a plot trusted then bypass the cooldown
                    if (plot != null && plot.getTrusted().contains(player.getUniqueId())) {
                        bypassedCooldown = true;
                        return true;
                    }
                case OWNER:
                    // If they are the owner or have bypass perms then bypass the cooldown
                    if (player.getUniqueId().equals(getInput().getOwner()) || player.hasPermission("syn.input.cooldown.bypass")) {
                        bypassedCooldown = true;
                        return true;
                    }
                // By default cancel the activation and send the player an error
                default:
                    String timeMessage = Message.getTimeMessage(remainingTime, 3, 0);
                    player.sendMessage(Message.format("inputs.cooldown.error.cooling_down", timeMessage));
                    return false;
            }
        }

        // If it wasn't a player that activated the input then just cancel the activation
        return false;
    }

    @Override
    public void onInputActivate(Entity entity) {
        // Send a warning to the player that activated the input if they bypassed the cooldown
        if (bypassedCooldown) {
            entity.sendMessage(Message.format("inputs.cooldown.warning.bypassed_cooldown"));
        }
    }

    @Override
    public void onInputLoad() {
        // Log the time this input was loaded so that <code>getRemainingTime</code> is calculated properly
        timeLoaded = System.currentTimeMillis();
    }

    @Override
    public void onInputUnload() {
        // Save the remaining time in the database so that it can be continued later
        timeRemainder = getRemainingTime();
        getInput().saveInputFunctions();
    }
}
