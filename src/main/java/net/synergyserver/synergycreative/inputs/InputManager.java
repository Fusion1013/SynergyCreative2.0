package net.synergyserver.synergycreative.inputs;

import net.minecraft.server.v1_14_R1.BlockPosition;
import net.minecraft.server.v1_14_R1.BlockPressurePlateAbstract;
import net.minecraft.server.v1_14_R1.BlockPressurePlateBinary;
import net.minecraft.server.v1_14_R1.BlockPressurePlateWeighted;
import net.minecraft.server.v1_14_R1.Entity;
import net.minecraft.server.v1_14_R1.IBlockData;
import net.minecraft.server.v1_14_R1.MovingObjectPositionBlock;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycreative.SynergyCreative;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.Switch;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.block.impl.CraftPressurePlateBinary;
import org.bukkit.craftbukkit.v1_14_R1.block.impl.CraftPressurePlateWeighted;
import org.bukkit.craftbukkit.v1_14_R1.util.CraftMagicNumbers;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.plugin.PluginManager;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Represents an <code>InputManager</code>
 */
public class InputManager {

    private static InputManager instance;
    private HashMap<String, HashMap<String, Input>> inputs = new HashMap<>();

    /**
     * Creates a new <code>InputManager</code> object.
     */
    private InputManager() {}

    /**
     * Returns the object representing this <code>InputManager</code>.
     *
     * @return The object of this class.
     */
    public static InputManager getInstance() {
        if (instance == null) {
            instance = new InputManager();
        }
        return instance;
    }

    /**
     * Caches all <code>Input</code>s from the database and performs an
     * integrity check on each, deleting corrupted data from the database.
     *
     * @return The number of corrupted entries that were removed.
     */
    public int cacheAndCleanse() {
        DataManager dm = DataManager.getInstance();
        // InputMaskManager imm = InputMaskManager.getInstance();
        List<Input> inputList = MongoDB.getInstance().getDatastore().find(Input.class).asList();

        int corrupted = 0;
        for (Input input : inputList) {
            try {
                Block block = input.getBlock();

                // If the block mismatches the stored material then delete the input and increment the corrupted counter
                if (!block.getType().equals(input.getBlockType())) {
                    dm.deleteDataEntity(input);
                    corrupted++;
                    continue;
                }

                String chunkID = BukkitUtil.chunkToString(block.getChunk());
                String inputID = input.getID();

                BlockData blockData = block.getBlockData();

                // If a collision is detected then resolve it and increment the corrupted counter
                if (inputs.containsKey(chunkID)) {
                    HashMap<String, Input> inputsForChunk = inputs.get(chunkID);
                    if (inputsForChunk.containsKey(inputID)) {
                        // Keep the more recently created input and delete the other from the database
                        if (inputsForChunk.get(inputID).getDateCreated() < input.getDateCreated()) {
                            dm.deleteDataEntity(inputsForChunk.get(inputID));
                            inputsForChunk.put(inputID, input);

                            // Save the direction if it is specified in the config
                            if (PluginConfig.getConfig(SynergyCreative.getPlugin()).getBoolean("save_direction")) {
                                if (blockData instanceof Directional) {
                                    input.setFacing(((Directional) blockData).getFacing());
                                }
                                if (blockData instanceof Switch) {
                                    input.setFace(((Switch) blockData).getFace());
                                }
                            }

                            // Update the mask for WorldEdit
                            // imm.addInput(input);
                        } else {
                            dm.deleteDataEntity(input);
                        }
                        corrupted++;
                    } else {
                        // Add the input since it wasn't found
                        inputsForChunk.put(inputID, input);
                        // Update the mask for WorldEdit
                        // imm.addInput(input);

                        // Save the direction if it is specified in the config
                        if (PluginConfig.getConfig(SynergyCreative.getPlugin()).getBoolean("save_direction")) {
                            if (blockData instanceof Directional) {
                                input.setFacing(((Directional) blockData).getFacing());
                            }
                            if (blockData instanceof Switch) {
                                input.setFace(((Switch) blockData).getFace());
                            }
                        }
                    }
                } else {
                    // Add the chunk and input since they weren't found
                    inputs.put(chunkID, new HashMap<>());
                    inputs.get(chunkID).put(inputID, input);

                    // Save the direction if it is specified in the config
                    if (PluginConfig.getConfig(SynergyCreative.getPlugin()).getBoolean("save_direction")) {
                        if (blockData instanceof Directional) {
                            input.setFacing(((Directional) blockData).getFacing());
                        }
                        if (blockData instanceof Switch) {
                            input.setFace(((Switch) blockData).getFace());
                        }
                    }

                    // Update the mask for WorldEdit
                    // imm.addInput(input);
                }
            } catch (Exception e) {
                Bukkit.getConsoleSender().sendMessage("Corrupted Input at " + input.getID());
            }
        }

        // Only convert data once
        if (PluginConfig.getConfig(SynergyCreative.getPlugin()).getBoolean("save_direction")) {
            PluginConfig.getConfig(SynergyCreative.getPlugin()).set("save_direction", false);
        }

        return corrupted;
    }

    /**
     * Gets all <code>Input</code>s registered.
     *
     * @return All <code>Input</code>s.
     */
    public HashMap<String, HashMap<String, Input>> getInputs() {
        return inputs;
    }


    public Input getInput(Block block) {
        return getInput(block.getLocation());
    }

    /**
     * Gets an <code>Input</code> by its location.
     *
     * @param loc The location of the <code>Input</code> to get.
     * @return The <code>Input</code> if found, or null.
     */
    public Input getInput(Location loc) {
        org.bukkit.Chunk chunk = loc.getChunk();
        HashMap<String, Input> inputsForChunk = inputs.get(BukkitUtil.chunkToString(chunk));
        return inputsForChunk == null ? null : inputsForChunk.get(BukkitUtil.blockLocationToString(loc));
    }

    /**
     * Adds the given <code>Input</code> to the cache and database if its location doesn't conflict with another.
     *
     * @param input The <code>Input</code> to save.
     */
    public void addInput(Input input) {
        String chunkID = BukkitUtil.chunkToString(input.getLocation().getChunk());

        // Create the HashMap for the chunk if it doesn't already exist
        if (!inputs.containsKey(chunkID)) {
            inputs.put(chunkID, new HashMap<>());
        }

        // Cache the Input and save it in the database
        inputs.get(chunkID).put(input.getID(), input);
        DataManager.getInstance().saveDataEntity(input);
        // Update the mask for WorldEdit
        // InputMaskManager.getInstance().addInput(input);
    }

    /**
     * Deletes the given <code>Input</code> from the cache and database if it is present.
     *
     * @param input The <code>Input</code> to delete.
     */
    public void deleteInput(Input input) {
        // Remove the Input from the cache
        String chunkID = BukkitUtil.chunkToString(input.getLocation().getChunk());
        HashMap<String, Input> inputsForChunk = inputs.get(chunkID);
        inputsForChunk.remove(input.getID());

        // If the chunk has no inputs then remove that HashMap from the global HashMap too
        if (inputsForChunk.isEmpty()) {
            inputs.remove(chunkID);
        }

        // Delete it in the database
        DataManager.getInstance().deleteDataEntity(input);
    }

    /**
     * Gets the <code>Input</code> at the specified location or creates one if it doesn't exist.
     *
     * @param block The block of the <code>Input</code> to get.
     * @param owner The UUID of the owner of the <code>Input</code> to create, if none doesn't already exist.
     * @return The <code>Input</code> at the given location, or null if the block isn't a valid type.
     */
    public Input getOrCreateInput(Block block, UUID owner) {
        if (getInput(block) != null) {
            return getInput(block);
        } else if (Input.isValidInput(block.getType())) {
            Input input = new Input(owner, block.getLocation());
            addInput(input);
            return input;
        } else {
            return null;
        }
    }

    /**
     * Manually activates the given input block with simulated default vanilla behavior.
     *
     * @param block The block of the input to activate.
     */
    public void activateInputInWorld(Block block) {
        // NMS shenanigans because Bukkit's API is incomplete
        Location loc = block.getLocation();
        org.bukkit.World world = loc.getWorld();

        net.minecraft.server.v1_14_R1.World nmsWorld = ((CraftWorld) world).getHandle();
        net.minecraft.server.v1_14_R1.Block nmsBlock = CraftMagicNumbers.getBlock(block.getType());
        BlockPosition blockPosition = new BlockPosition(loc.getX(), loc.getY(), loc.getZ());
        IBlockData iBlockData = nmsWorld.getType(blockPosition);;

        BlockData blockData = block.getBlockData();
        // Activate the input in the world
        switch (block.getType()) {
            case STONE_BUTTON:
            case OAK_BUTTON:
            case BIRCH_BUTTON:
            case SPRUCE_BUTTON:
            case JUNGLE_BUTTON:
            case DARK_OAK_BUTTON:
            case ACACIA_BUTTON:
            case LEVER:
                // Use the provided method for buttons and levers
                iBlockData.interact(nmsWorld, null, null, new MovingObjectPositionBlock(null, null, blockPosition, false));
                break;
            case STONE_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case DARK_OAK_PRESSURE_PLATE:
            case ACACIA_PRESSURE_PLATE:
            case HEAVY_WEIGHTED_PRESSURE_PLATE:
            case LIGHT_WEIGHTED_PRESSURE_PLATE:
                BlockPressurePlateAbstract pressurePlate = (BlockPressurePlateAbstract) nmsBlock;
                pressurePlate.a(iBlockData, nmsWorld, blockPosition, (Entity) null);

                PluginManager manager = nmsWorld.getServer().getPluginManager();
                int oldCurrent = 0;
                if (blockData instanceof CraftPressurePlateBinary) {
                    oldCurrent = ((CraftPressurePlateBinary) blockData).isPowered() ? 15 : 0;
                } else if (blockData instanceof CraftPressurePlateWeighted) {
                    oldCurrent = ((CraftPressurePlateWeighted) blockData).getPower();
                }
                boolean wasPowered = oldCurrent > 0;

                BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(world.getBlockAt(blockPosition.getX(), blockPosition.getY(), blockPosition.getZ()), oldCurrent, 15);
                manager.callEvent(eventRedstone);
                int newCurrent = eventRedstone.getNewCurrent();
                boolean willBePowered = eventRedstone.getNewCurrent() > 0;

                if (oldCurrent != newCurrent) {
                    IBlockData newiBlockData = iBlockData;
                    if (pressurePlate instanceof BlockPressurePlateBinary) {
                        newiBlockData = iBlockData.set(BlockPressurePlateBinary.POWERED, newCurrent > 0);
                    } else if (pressurePlate instanceof BlockPressurePlateWeighted) {
                        newiBlockData = iBlockData.set(BlockPressurePlateWeighted.POWER, newCurrent);
                    }

                    nmsWorld.setTypeAndData(blockPosition, newiBlockData, 2);
                    nmsWorld.applyPhysics(blockPosition, nmsBlock);
                    nmsWorld.applyPhysics(blockPosition.down(), nmsBlock);
                    nmsWorld.b(blockPosition, iBlockData, newiBlockData);
                }

                if (!willBePowered && wasPowered) {
                    if (pressurePlate instanceof BlockPressurePlateBinary) {
                        if (block.getType().equals(org.bukkit.Material.STONE_PRESSURE_PLATE)) {
                            world.playSound(loc.add(0.5, 0.5, 0.5), Sound.BLOCK_STONE_PRESSURE_PLATE_CLICK_OFF, 0.3F, 0.5F);
                        } else {
                            world.playSound(loc.add(0.5, 0.5, 0.5), Sound.BLOCK_WOODEN_PRESSURE_PLATE_CLICK_OFF, 0.3F, 0.7F);
                        }
                    } else if (pressurePlate instanceof BlockPressurePlateWeighted) {
                        world.playSound(loc.add(0.5, 0.5, 0.5), Sound.BLOCK_METAL_PRESSURE_PLATE_CLICK_OFF, 0.3F, 0.75F);
                    }
                } else if (willBePowered && !wasPowered) {
                    if (pressurePlate instanceof BlockPressurePlateBinary) {
                        if (block.getType().equals(org.bukkit.Material.STONE_PRESSURE_PLATE)) {
                            world.playSound(loc.add(0.5, 0.5, 0.5), Sound.BLOCK_STONE_PRESSURE_PLATE_CLICK_ON, 0.3F, 0.6F);
                        } else {
                            world.playSound(loc.add(0.5, 0.5, 0.5), Sound.BLOCK_WOODEN_PRESSURE_PLATE_CLICK_ON, 0.3F, 0.8F);
                        }
                    } else if (pressurePlate instanceof BlockPressurePlateWeighted) {
                        world.playSound(loc.add(0.5, 0.5, 0.5), Sound.BLOCK_METAL_PRESSURE_PLATE_CLICK_ON, 0.3F, 0.90000004F);
                    }
                }

                if (willBePowered) {
                    nmsWorld.getBlockTickList().a(new BlockPosition(blockPosition), pressurePlate, pressurePlate.a(nmsWorld));
                }
                break;
            default:
                break;
        }
    }
}
