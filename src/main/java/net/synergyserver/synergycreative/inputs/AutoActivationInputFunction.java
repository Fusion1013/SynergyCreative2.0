package net.synergyserver.synergycreative.inputs;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergycreative.SynergyCreative;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitTask;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

@Embedded
public class AutoActivationInputFunction extends InputFunction {

    @Property("d")
    private int delay;
    @Property("a")
    private boolean willActivate;
    @Property("tr")
    private long timeRemainder;

    @Transient
    private BukkitTask pendingActivation;
    @Transient
    private long timeLoaded;

    /**
     * Required constructor for Morphia to work.
     */
    public AutoActivationInputFunction() {}

    /**
     * Creates a new <code>AutoActivationInputFunction</code> with the given parameters.
     *
     * @param input The <code>Input</code> that this <code>AutoActivationInputFunction</code> is registered to.
     * @param delay The time in milliseconds that this <code>AutoActivationInputFunction</code> waits before auto-activating.
     */
    public AutoActivationInputFunction(Input input, int delay) {
        super(input);
        this.delay = delay;
        this.willActivate = false;
        this.timeRemainder = 0;
    }

    /**
     * Gets the time remaining in milliseconds until this <code>AutoActivationInputFunction</code>'s automatically activates.
     * This method should only be called after it is verified that the input should automatically activate in the
     * first place as no such check occurs in this method.
     *
     * @return The time until the input automatically activates. Returns 0 if this input should (have) activate(d).
     */
    public long getRemainingTime() {
        long currentTime = System.currentTimeMillis();
        long timeSinceActivation = currentTime - getInput().getLastUsed();
        long timeSinceLoad = currentTime - timeLoaded;

        // Return whichever has more delay: The time remainder from being unloaded or the time remaining from normal activation
        long maxRemainingTime = Math.max(timeRemainder - timeSinceLoad, delay - timeSinceActivation);
        return Math.max(maxRemainingTime, 0);
    }

    @Override
    public String getType() {
        return "autoactivation";
    }

    @Override
    public String getDescription() {
        long delayInTicks = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, delay);

        if (willActivate) {
            long remainingTimeInTicks = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, getRemainingTime());
            return Message.format("inputs.autoactivation.description_will_activate", delayInTicks, remainingTimeInTicks);
        }
        return Message.format("inputs.autoactivation.description_will_not_activate", delayInTicks);
    }

    @Override
    public InputPriorityLevel getPriority() {
        return InputPriorityLevel.LOW;
    }

    /**
     * Sets whether this <code>AutoActivationInputFunction</code> will automatically activate after the next activation.
     *
     * @param willActivate Whether this <code>AutoActivationInputFunction</code> should activate.
     */
    public void setWillActivate(boolean willActivate) {
        this.willActivate = willActivate;
        getInput().saveInputFunctions();
    }

    @Override
    public void onInputActivate(Entity entity) {
        Input input = getInput();

        // Ignore if the Input is a pressure plate, as plates' delays start after they deactivate instead
        switch (input.getBlockType()) {
            case STONE_BUTTON:
            case OAK_BUTTON:
            case BIRCH_BUTTON:
            case SPRUCE_BUTTON:
            case JUNGLE_BUTTON:
            case DARK_OAK_BUTTON:
            case ACACIA_BUTTON:
            case LEVER:
                break;
            default:
                return;
        }

        // Toggle willActivate
        setWillActivate(!willActivate);

        // Return if this InputFunction shouldn't autoactivate after this activation
        if (!willActivate) {
            return;
        }

        // If there is already a pending activation then cancel it
        if (pendingActivation != null) {
            pendingActivation.cancel();
            pendingActivation = null;
        }

        // Schedule an autoactivation
        pendingActivation = Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // If the input has since been activated since the creation of the task and an autoactivation is
                // no longer necessary then cancel the task
                if (!willActivate) {
                    return;
                }

                // If another InputFunction is blocking the activation of the Input then cancel the task
                if (!input.shouldActivate(null, null)) {
                    return;
                }

                // Activate the input in the world
                InputManager.getInstance().activateInputInWorld(input.getBlock());

                // Force activate the Input so other Inputs can hook into this activation
                input.activate(null);
            }
        }, TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, delay));
    }

    @Override
    public void onInputDeactivate() {
        Input input = getInput();

        // Ignore if the Input isn't a pressure plate
        switch (input.getBlockType()) {
            case STONE_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case DARK_OAK_PRESSURE_PLATE:
            case ACACIA_PRESSURE_PLATE:
                break;
            default:
                return;
        }

        // Toggle willActivate
        setWillActivate(!willActivate);

        // Return if this InputFunction shouldn't autoactivate after this activation
        if (!willActivate) {
            return;
        }

        // If there is already a pending activation then cancel it
        if (pendingActivation != null) {
            pendingActivation.cancel();
            pendingActivation = null;
        }

        // Schedule an autoactivation
        pendingActivation = Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // If the input has since been activated since the creation of the task and an autoactivation is
                // no longer necessary then cancel the task
                if (!willActivate) {
                    return;
                }

                // If another InputFunction is blocking the activation of the Input then cancel the task
                if (!input.shouldActivate(null, null)) {
                    return;
                }

                // Activate the input in the world
                InputManager.getInstance().activateInputInWorld(input.getBlock());

                // Force activate the Input so other Inputs can hook into this activation
                input.activate(null);
            }
        }, TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, delay));
    }

    @Override
    public void onInputLoad() {
        timeLoaded = System.currentTimeMillis();

        // If the input is to be automatically activated then schedule a task
        if (!willActivate) {
            return;
        }

        Input input = getInput();

        // Schedule an autoactivation
        pendingActivation = Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // If the input has since been activated since the creation of the task and an autoactivation is
                // no longer necessary then cancel the task
                if (!willActivate) {
                    return;
                }

                // If another InputFunction is blocking the activation of the Input then cancel the task
                if (!input.shouldActivate(null, null)) {
                    return;
                }

                // Activate the input in the world and clear the timeRemainder
                InputManager.getInstance().activateInputInWorld(input.getBlock());
                timeRemainder = 0;
                getInput().saveInputFunctions();

                // Force activate the Input so other Inputs can hook into this activation
                input.activate(null);
            }
        }, TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, timeRemainder));
    }

    @Override
    public void onInputUnload() {
        // Stop the task if it exists
        if (pendingActivation != null) {
            pendingActivation.cancel();
            pendingActivation = null;
        }

        // Save the remaining time in the database so that it can be continued later
        timeRemainder = getRemainingTime();
        getInput().saveInputFunctions();
    }
}
