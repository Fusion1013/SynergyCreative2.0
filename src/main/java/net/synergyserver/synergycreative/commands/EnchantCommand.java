package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "enchanttable",
        aliases = {"enchtable"},
        permission = "syn.enchant",
        usage = "/enchant",
        description = "Opens the enchantment table user interface.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)


public class EnchantCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        player.openEnchanting(null, true);

        return true;
    }
}
