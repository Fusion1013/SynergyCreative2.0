package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergycreative.inputs.AutoActivationInputFunction;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputFunction;
import net.synergyserver.synergycreative.inputs.InputManager;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.UUID;

@CommandDeclaration(
        commandName = "autoactivation",
        aliases = {"auto", "aa", "autoactivated"},
        permission = "syn.input.add.autoactivation",
        usage = "/input add autoactivation <delay in ticks>",
        description = "Adds an automatic reactivation with the given delay to the input you are looking at. " +
                "Only works properly with two-state creations",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "input add"
)
public class InputAddAutoActivationCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID pID = player.getUniqueId();

        // If the delay isn't a valid integer then give the player an error
        if (!MathUtil.isInteger(args[0])) {
            player.sendMessage(Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        // If the delay is too big or is negative then give the player an error
        int delayInTicks = Integer.parseInt(args[0]);
        if (delayInTicks < 1 || delayInTicks > 1000000) {
            player.sendMessage(Message.format("commands.error.out_of_range", 1, 1000000, args[0]));
            return false;
        }

        // If the player isn't looking at an input then give the player an error
        Block block = player.getTargetBlock((Set<Material>) null, 10);
        if (block == null || !Input.isValidInput(block.getType())) {
            player.sendMessage(Message.format("commands.input.error.input_not_found"));
            return false;
        }

        // If the player cannot build where the Input is being created then give the player an error
        if (!PlotUtil.canBuild(player, block.getLocation())) {
            player.sendMessage(Message.format("commands.input.add.error.no_permission"));
            return false;
        }

        // Get the Input at the block's location
        Input input = InputManager.getInstance().getOrCreateInput(block, pID);

        // If they can't modify the input then give the player an error
        if (!PlotUtil.canBuild(player, input.getLocation())) {
            player.sendMessage(Message.format("commands.input.error.no_modify_permission"));
            return false;
        }

        // If the Input already has a AutoActivationInputFunction registered then give the player an error
        for (InputFunction inputFunction : input.getInputFunctions()) {
            if (inputFunction instanceof AutoActivationInputFunction) {
                player.sendMessage(Message.format("commands.input.add.error.function_already_exists", inputFunction.getType()));
                return false;
            }
        }

        // Create a AutoActivationInputFunction and register it to the Input
        int delayInMillis =  (int) TimeUtil.TimeUnit.GAME_TICK.getMilliseconds() * delayInTicks;
        AutoActivationInputFunction autoActivatedInput = new AutoActivationInputFunction(input, delayInMillis);
        input.addInputFunction(autoActivatedInput);

        // Send the player a success message
        player.sendMessage(Message.format("commands.input.add.autoactivation.info.created_input", delayInTicks));
        return true;
    }
}
