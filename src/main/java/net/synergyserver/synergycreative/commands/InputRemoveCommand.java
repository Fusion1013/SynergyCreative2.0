package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputFunction;
import net.synergyserver.synergycreative.inputs.InputManager;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Set;

@CommandDeclaration(
        commandName = "remove",
        aliases = {"delete", "del"},
        permission = "syn.input.remove",
        usage = "/input remove [function type]",
        description = "Removes functionality from the input that you are looking at. If a type is specified then only that functionality will be removed.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "input"
)
public class InputRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // If the player isn't looking at an input then give the player an error
        Block block = player.getTargetBlock((Set<Material>) null, 10);
        if (block == null || !Input.isValidInput(block.getType())) {
            player.sendMessage(Message.format("commands.input.error.input_not_found"));
            return false;
        }

        // Get the Input at that location and give the player an error if none was found
        Input input = InputManager.getInstance().getInput(block);
        if (input == null) {
            player.sendMessage(Message.format("commands.input.error.no_inputs"));
            return true;
        }

        boolean bypassing = player.hasPermission("syn.input.remove.bypass") && !PlotUtil.canBuild(player, input.getLocation());

        // If they can't build and don't have bypass perms then give the player an error
        if (!PlotUtil.canBuild(player, input.getLocation()) && !bypassing) {
            player.sendMessage(Message.format("commands.input.error.no_modify_permission"));
            return false;
        }

        // If the player didn't specify an input type then delete the entire Input and give them feedback
        if (args.length == 0) {
            InputManager.getInstance().deleteInput(input);
            player.sendMessage(Message.format("commands.input.remove.info.removed_all"));

            // Warn the player if they are bypassing the owner restriction to remove the input
            if (bypassing) {
                player.sendMessage(Message.format("commands.input.remove.warning.bypassed_ownership"));
            }
            return true;
        }

        String inputType = args[0].toLowerCase();

        HashMap<String, InputFunction> inputs = new HashMap<>();
        for (InputFunction inputFunction : input.getInputFunctions()) {
            inputs.put(inputFunction.getType(), inputFunction);
        }
        Set<String> inputTypes = inputs.keySet();
        String inputTypesList = Message.createFormattedList(
                inputTypes,
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.item_color_2"),
                Message.get("info_colored_lists.grammar_color")
        );

        // If they didn't provide an input type that the Input has a registered input of then give the player an error
        if (!inputTypes.contains(inputType)) {
            player.sendMessage(Message.format("commands.input.error.invalid_function_type", inputType, inputTypesList));
            return false;
        }

        // Delete the specified input and give the player feedback
        input.removeInputFunction(inputs.get(inputType));
        player.sendMessage(Message.format("commands.input.remove.info.removed_one", inputType));

        // Warn the player if they are bypassing the owner restriction to remove the input
        if (bypassing) {
            player.sendMessage(Message.format("commands.input.remove.warning.bypassed_ownership"));
        }

        // If no functions remain then remove the Input itself from the database
        if (input.getInputFunctions().size() == 0) {
            InputManager.getInstance().deleteInput(input);
        }
        return true;
    }
}
