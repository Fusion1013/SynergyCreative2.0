package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.MathUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;

@CommandDeclaration(
        commandName = "power",
        aliases = "pow",
        permission = "syn.firework.power",
        usage = "/firework power [value]",
        description = "Alters the power of the firework in a player's hand",
        maxArgs = 1,
        parentCommandName = "firework"
)

public class FireworkPowerCommand extends SubCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        PlayerInventory inventory = player.getInventory();

        ItemStack fireworkItem = inventory.getItemInMainHand();
        FireworkMeta fireworkMeta = (FireworkMeta) fireworkItem.getItemMeta();

        if(!fireworkItem.getType().equals(Material.FIREWORK_ROCKET)){
            player.sendMessage(Message.get("commands.firework.error.no_firework"));
            return false;
        }

        //check if power is an integer
        if(!MathUtil.isInteger(args[0])){
            player.sendMessage(Message.format("commands.firework.power.error.not_an_integer", args[0]));
            return false;
        }

        int power = Integer.parseInt(args[0]);

        //bound check power
        if(power < 0 || power > 128){
            player.sendMessage(Message.format("commands.error.out_of_range",0,128));
            return false;
        }

        //set power and alert player
        fireworkMeta.setPower(power);
        fireworkItem.setItemMeta(fireworkMeta);
        player.sendMessage(Message.format("commands.firework.power.info.set_power", power));
        return true;
    }
}
