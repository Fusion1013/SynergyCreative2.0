package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergycreative.inputs.CooldownInputFunction;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputFunction;
import net.synergyserver.synergycreative.inputs.InputManager;
import net.synergyserver.synergycreative.inputs.UserGroup;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.UUID;

@CommandDeclaration(
        commandName = "cooldown",
        aliases = "cd",
        permission = "syn.input.add.cooldown",
        usage = "/input add cooldown <time in ticks> [owner|plot_trusted|plot_members]",
        description = "Adds a cooldown with the given time to the input you are looking at. " +
                "Players in the given group can bypass this delay.",
        minArgs = 1,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "input add"
)
public class InputAddCooldownCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID pID = player.getUniqueId();

        // If the time isn't a valid integer then give the player an error
        if (!MathUtil.isInteger(args[0])) {
            player.sendMessage(Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        // If the delay is too big or is negative then give the player an error
        int timeInTicks = Integer.parseInt(args[0]);
        if (timeInTicks < 1 || timeInTicks > 1000000) {
            player.sendMessage(Message.format("commands.error.out_of_range", 1, 1000000, args[0]));
            return false;
        }

        // If a bypass group was given and it isn't valid then give the player an error
        UserGroup bypassGroup = UserGroup.NOBODY;
        if (args.length == 2) {
            switch (args[1].toLowerCase()) {
                case "owner":
                case "self":
                    bypassGroup = UserGroup.OWNER;
                    break;
                case "plot_trusted":
                case "trusted":
                    bypassGroup = UserGroup.PLOT_TRUSTED;
                    break;
                case "plot_members":
                case "members":
                    bypassGroup = UserGroup.PLOT_MEMBERS;
                    break;
                default:
                    player.sendMessage(Message.format("commands.input.add.error.invalid_user_group", args[1]));
                    return false;
            }
        }

        // If the player isn't looking at an input then give the player an error
        Block block = player.getTargetBlock((Set<Material>) null, 10);
        if (block == null || !Input.isValidInput(block.getType())) {
            player.sendMessage(Message.format("commands.input.error.input_not_found"));
            return false;
        }

        // If the player cannot build where the Input is being created then give the player an error
        if (!PlotUtil.canBuild(player, block.getLocation())) {
            player.sendMessage(Message.format("commands.input.add.error.no_permission"));
            return false;
        }

        // Get the Input at the block's location
        Input input = InputManager.getInstance().getOrCreateInput(block, pID);

        // If they can't modify the input then give the player an error
        if (!PlotUtil.canBuild(player, input.getLocation())) {
            player.sendMessage(Message.format("commands.input.error.no_modify_permission"));
            return false;
        }

        // If the Input already has a CooldownInputFunction registered then give the player an error
        for (InputFunction inputFunction : input.getInputFunctions()) {
            if (inputFunction instanceof CooldownInputFunction) {
                player.sendMessage(Message.format("commands.input.add.error.function_already_exists", inputFunction.getType()));
                return false;
            }
        }

        // Create a CooldownInputFunction and register it to the Input
        int timeInMillis = (int) TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.GAME_TICK, TimeUtil.TimeUnit.MILLI, timeInTicks);
        CooldownInputFunction cooldownInput = new CooldownInputFunction(input, timeInMillis, bypassGroup);
        input.addInputFunction(cooldownInput);

        // Send the player a success message
        if (bypassGroup.equals(UserGroup.NOBODY)) {
            player.sendMessage(Message.format("commands.input.add.cooldown.info.created_no_bypass", timeInTicks));
        } else {
            player.sendMessage(Message.format("commands.input.add.cooldown.info.created_with_bypass", timeInTicks, bypassGroup.toString()));
        }
        return true;
    }
}
