package net.synergyserver.synergycreative;

import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import org.mongodb.morphia.annotations.Entity;

/**
 * Represents the profile of a player in the Creative world group.
 */
@Entity(value = "worldgroupprofiles")
public class CreativeWorldGroupProfile extends WorldGroupProfile {

    /**
     * Required constructor for Morphia to work.
     */
    public CreativeWorldGroupProfile() {}

    /**
     * Creates a new <code>CreativeWorldGroupProfile</code> with the given parameters.
     *
     * @param wgp The <code>WorldGroupProfile</code> to extend.
     */
    public CreativeWorldGroupProfile(WorldGroupProfile wgp) {
        super(wgp.getWorldGroupName(), wgp.getPlayerID(), wgp.getLastLogIn(), wgp.getLastLogOut(),
                wgp.getLastLocation(), wgp.getPrefID());
    }

}
